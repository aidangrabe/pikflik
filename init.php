<?php
/**
 * init.php
 * @author		Aidan Grabe
 * @version		20130102
 * @since		20121205
 * Setup the path and root constants
 * PATH = the absolute path to the project's base directory
 * ROOT = the relative root to the project's base directory
 */

/**
 * Works out the root directory of the project relative to the hostname
 * @return string the root directory path
 * @since 20121205
 */
function getRootDir( ) {
	//return "/ABSOLUTE_RELATIVE_PATH_TO_CONTAINING FOLDER/";
	// if you do not use sub domain directories, comment out the line above
	return substr( PATH, strlen( $_SERVER['DOCUMENT_ROOT']), strlen( PATH));
}

// define the project's title
define( "PROJECT_TITLE", "PikFlik");

// gives an absolute path to project directory with a trailing '/' ( slash)
define( "PATH", dirname( __FILE__) . '/');
// gives a relative path to the project directory with a trailing slash
// does not explicitly look like it because it uses the PATH constant which
// already contains a trailing slash
define( "ROOT", getRootDir());

// define the absolute paths to the php scripts
define( "PATH_LIB", PATH . "php/lib/");
define( "PATH_PIK", PATH . "php/pikflik/");

// define the links to the stylesheets
define( "CSS_ADMIN", ROOT . "css/admin.css");

// define the image locations
define( "IMG_ROOT", "images/");
define( "IMG_SOURCE", IMG_ROOT . "source/");
define( "IMG_THUMBNAIL", IMG_ROOT . "thumbnails/");
define( "IMG_DISPLAY", IMG_ROOT . "display/");
define( "IMG_LOGO", ROOT . "css/img/icon.png");

// define theme locations
define( "THEME_ROOT", "themes/");

// define the location to the favicon
define( "PIKFLIK_FAVICON", ROOT . "css/img/favicon.ico");

// get the user configurations
require_once( "config/config.php");

?>