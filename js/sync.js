/**
 * @author		Aidan Grabe
 * @version		20121216
 * @since		20121205
 */ 

function Sync( imgToCreate) {
	var imagesToCreate = imgToCreate.length + 1;
	var currentImage = 0;
	var loadingBar = $( '#sync-container .loading div');
	var ready = true;
	var that = this;
	var finishMethod = function() {};
	
	this.getPercentage = function() {
		var p = 0;
		if ( imagesToCreate > 0) {
			p = currentImage / imagesToCreate * 100;
		}
		return p;
	}
	
	this.isReady = function() {
		return ready;
	}
	
	this.next = function( data) {
		currentImage++;
		that.update();
		if ( currentImage == imagesToCreate) {
			ready = true;
			that.finishMethod();
			return;
		}
		$.get( ROOT + 'php/pikflik/ajax.php?action=create&src=' + imgToCreate[ currentImage - 1], function( data) {
						that.next( data);
					});
	}
	
	this.removeImages = function() {
		$.get( ROOT + 'php/pikflik/ajax.php?action=deleteImages', this.next);
	}
	
	this.start = function( callBack) {
		ready = false;
		this.finishMethod = callBack;
		this.removeImages();
	}
	
	this.update = function() {
		loadingBar.stop();
		loadingBar.animate( { width: this.getPercentage() + '%'});
	}
}

$( document).ready( function() {
	
	$("#sync-container .loading").css( { display: 'none'});
	
	var syncButton = "#sync-button";
	$(syncButton).click( function() {
		$("#sync-container .loading").fadeIn( 'fast');
		var sync = new Sync( imagesToCreate);
		$(syncButton).attr( "value", "Sync");
		sync.start( function() {
			$(syncButton).attr( 'value', 'Done!');
		});
	});
	
	// shows the number of images to create on the sync button to show user sync is required.
	var numImagesToCreate = window.imagesToCreate.length;
	if (  numImagesToCreate > 0) {
		$(syncButton).attr( "value", $(syncButton).attr( "value") + " (" + numImagesToCreate + ")");
	}
	
});