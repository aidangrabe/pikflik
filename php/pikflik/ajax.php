<?php
/**
 * ajax.php
 * @author		Aidan Grabe
 * @version		20130315
 * @since		20121207
 * Answers ajax calls
 */

require_once( "../../init.php");
require_once( "../../config/config.php");
require_once( PATH_LIB . "Data.class.php");
require_once( PATH_LIB . "Image.class.php");
require_once( PATH_PIK . "Sync.class.php");

$data = new Data( $_GET);
$action = $data->getRequiredString( 'action', 0, 100);
if ( $action == null) {
	echo "ERROR";
	die();
} else {
	switch ( $action) {
		default:
			echo "ERROR";
			die();
			break;
		case 'deleteImages':
			$sync = new Sync();
			$sync->removeHangingImages( );
			echo "0";
			break;
		case 'create':
			$src = $data->getRequiredString( 'src', 0, 255);
			//$src = str_replace( " ", "%20", $src);
			$image = new Image(  PATH . IMG_SOURCE . $src);
			$thumb = $image->scaleToWidth( THUMBNAIL_WIDTH);
			if ( !$thumb->hasErrors()) {
				$thumb->save( PATH . IMG_THUMBNAIL . $src);
			}
			$display = $image->scaleToWidth( DISPLAY_WIDTH);

			// draw the copyright on the display image
			
			// set the font color of the image
			$display->setFontColor(0, 0, 0, IMAGE_FONT_ALPHA);
			
			// draw the tex on the image
			$display->addText( $display->getWidth() - 5, $display->getHeight() - 
					5, IMAGE_FONT_SIZE, "© " . IMAGE_COPYRIGHT_TEXT . " " . date( "Y"),
					IMAGE_FONT, 'right');
			if ( !$display->hasErrors()) {
				$display->save( PATH . IMG_DISPLAY . $src);
			}
			echo "0";
			break;
	}
}

?>