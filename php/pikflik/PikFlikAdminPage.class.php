<?php
/**
 * php/pikflik/PikFlikAdminPage.class.php
 * @author		Aidan Grabe
 * @version		20121217
 * @since		20121205
 * An extension of the /php/lib/Page.class.php class
 */
 
require_once( PATH_LIB . "Page.class.php");
 
class PikFlikAdminPage extends Page {
	
	public function PikFlikAdminPage( $title) {
		parent::__construct( $title);
		$this->addCSS( CSS_ADMIN);
		$this->setFavicon( PIKFLIK_FAVICON);
	}
	
	public function outputFooter() {
		echo "
				</div> <!-- #main-content -->
				<footer>
					<small>Copyright PikFlik &copy; " . date( 'Y') . "</small>
				</footer>
			</div> <!-- #wrapper -->
		";
		parent::outputFooter();
	}
	
	public function outputHeader() {
		parent::outputHeader();
		echo "
			<div id=\"wrapper\">
				<header>
					<img src=\"" . IMG_LOGO . "\" alt=\"PikFlik Logo\" />
					<h1>" . PROJECT_TITLE . "</h1>
				</header>
				<div id=\"main-content\">
		";
	}
	
}
 
?>