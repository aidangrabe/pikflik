<?php
/**
 * Theme.class.php
 * @author		Aidan Grabe
 * @version		20121208
 * @since		20121208
 * The theme class
 */

class Theme {
	
	private $name;
	
	public function Theme( $name) {
		$this->name = $name;
		define( "ROOT_THEME", ROOT  . THEME_ROOT . $name . "/");
		define( "PATH_THEME", PATH . THEME_ROOT . $name . "/");
	}
	
	public function output( ) {
		require_once( PATH . THEME_ROOT . $this->name . "/index.php");
	}
	
}

?>