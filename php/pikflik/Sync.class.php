<?php
/**
 * Sync.class.php
 * @author		Aidan Grabe
 * @version		20121207
 * @since		20121207
 * Sync object
 */


class Sync {
	
	public function Sync() {
		
	}
	
	public static function getImages( $imageType) {
		$images = glob( PATH . $imageType . "*.{jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG}", GLOB_BRACE);
		$imageFilenames = array();
		foreach ( $images as $i => $image) {
			$imageFilenames[ $i] = basename( $image);
		}
		return $imageFilenames;
	}
	
	public function removeHangingImages() {
		$imgSource = $this->getImages( IMG_SOURCE);
		$imgThumbnail = $this->getImages( IMG_THUMBNAIL);
		$imgDisplay = $this->getImages( IMG_DISPLAY);
		
		$hangingImagesDisplay = $this->getHangingImages( $imgSource, $imgDisplay);
		$hangingImagesThumbnail = $this->getHangingImages( $imgSource, $imgThumbnail);
		
		foreach ( $hangingImagesDisplay as $h) {
			$this->removeImage( $h);
		}
		foreach ( $hangingImagesThumbnail as $h) {
			$this->removeImage( $h);
		}
	}
	
	private function removeImage( $name) {
		$file1 = PATH . IMG_DISPLAY . $name;
		$file2 = PATH . IMG_THUMBNAIL . $name;
		if ( file_exists( $file1)) {
			unlink( $file1);
		}
		if ( file_exists( $file2)) {
			unlink( $file2);
		}
	}
	
	private function getHangingImages( $src, $check) {
		$hangingImages = array();
		foreach ( $check as $checkImage) {
			$exists = false;
			foreach ( $src as $sourceImage) {
				if ( $sourceImage == $checkImage) {
					$exists = true;
					break;
				}
			}
			if ( ! $exists) {
				$hangingImages[] = $checkImage;
			}
		}
		return $hangingImages;
	}
	
	public function getImagesToCreate() {
		$imgSource = $this->getImages( IMG_SOURCE);
		$imgThumbnail = $this->getImages( IMG_THUMBNAIL);
		$imgDisplay = $this->getImages( IMG_DISPLAY);
		$imagesToCreate = array();
		
		foreach ( $imgSource as $source) {
			$exists = false;
			foreach ( $imgThumbnail as $thumbnail) {
				if ( $source == $thumbnail) {
					$exists = true;
					break;
				}
			}
			if ( !$exists) {
				foreach ( $imgDisplay as $display) {
					if ( $source == $display) {
						$exists = true;
						break;
					}
				}
			}
			if ( ! $exists) {
				$imagesToCreate[] = $source;
			}
		}
		return $imagesToCreate;
	}
	
}

?>