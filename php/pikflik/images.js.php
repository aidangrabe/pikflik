<?php
/**
 * init.js.php
 * @author		Aidan Grabe
 * @version		20121206
 * @since		20121206
 * Creates a javascript file containing the images and constants
 */
 
require_once( "../../init.php");
require_once( PATH_LIB . "Data.class.php");
require_once( PATH_PIK . "Sync.class.php");

function getImages( ) {
	$images = glob( PATH . IMG_SOURCE . "*.{jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG}", GLOB_BRACE);
	$imageFilenames = array();
	foreach ( $images as $i => $image) {
		$imageFilenames[ $i] = basename( $image);
	}
	return $imageFilenames;
}

function arrayToJS( $key, $array) {
	$output = "";
	if ( $array == null) {
		echo "var {$key} = [];";
		return;
	}
	$output .=  "var {$key} = [";
	foreach ( $array as $i => $url) {
		$output .= "\"{$url}\"";
		$output .= $i == count( $array) - 1
					? ""
					: ",";
	}
	$output .= "];";
	return $output;
}

// set the mime-type to javascript
header( "Content-Type: text/javascript");

$sync = new Sync();

echo "
	var PATH = '" . PATH . "';
	var ROOT = '" . ROOT . "';
	var IMG_ROOT = '" . IMG_ROOT . "';
	var IMG_SOURCE = '" . IMG_SOURCE . "';
	var IMG_DISPLAY = '" . IMG_DISPLAY . "';
	var IMG_THUMBNAIL = '" . IMG_THUMBNAIL . "';
";

echo arrayToJS( 'images', getImages( ));
echo arrayToJS( 'imagesToCreate', $sync->getImagesToCreate());

?>