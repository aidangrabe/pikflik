<?php
/**
 * theme.js.php
 * @author		Aidan Grabe
 * @version		20121221
 * @since		20121208
 */

require_once( "../../init.php");
require_once( PATH_PIK . "Sync.class.php");
require_once( PATH_LIB . "Data.class.php");

function arrayToJS( $key, $path, $array) {
	$output = "";
	if ( $array == null) {
		echo "var {$key} = [];";
		return;
	}
	$output .=  "var {$key} = [";
	foreach ( $array as $i => $url) {
		$output .= "\"{$path}{$url}\"";
		$output .= $i == count( $array) - 1
					? ""
					: ",";
	}
	$output .= "];";
	return $output;
}

header( "Content-Type: text/javascript");

$thumbnails = Sync::getImages( IMG_THUMBNAIL);
$display = Sync::getImages( IMG_DISPLAY);
$data = new Data( $_GET);
$currentImage = $data->getOptionalString( 'img', 0, 500, "");

echo "
	var HOST = '" . $_SERVER['HTTP_HOST'] . "';
	var ROOT = '" . ROOT . "';
	var PATH = '" . PATH . "';
	var IMG_ROOT = '" . IMG_ROOT . "';
	var IMG_SOURCE = '" . IMG_SOURCE . "';
	var IMG_DISPLAY = '" . IMG_DISPLAY . "';
	var IMG_THUMBNAIL = '" . IMG_THUMBNAIL . "';
	var IMG_CURRENT = '{$currentImage}';
";

echo arrayToJS( 'images_thumbnails', ROOT . IMG_THUMBNAIL, $thumbnails);
echo arrayToJS( 'images_display', ROOT . IMG_DISPLAY, $display);

?>