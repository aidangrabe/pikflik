<?php
/**
 * Form.class.php
 * @author		Aidan Grabe
 * @version		20121130
 * @since		20121130
 */

class Form {
	
	private $action;
	private $fields;
	private $method;
	private $resetButtonText;
	private $submitButtonText;
	
	public function Form( $action, $method) {
		$this->action = $action;
		$this->fields = array();
		$this->method = $method;
		$this->resetButtonText = "Reset";
		$this->submitButtonText = "Submit";
	}
	
	public function addTextField( $label, $name, $id, $value = "") {
		$this->fields[] = "
			<label>{$label}</label>
			<div><textarea name=\"{$name}\" id=\"{$id}\">{$value}</textarea></div>
			";
	}
	
	public function addTextInput( $label, $name, $id, $maxLength, $value = "") {
		$this->fields[] = "
			<label>{$label}</label>
			<div><input type=\"text\" name=\"{$name}\" id=\"{$id}\" maxlength=\"{$maxLength}\" value=\"{$value}\" /></div>
			";
	}
	
	public function display() {
		echo "<form action=\"{$this->action}\" method=\"{$this->method}\">";
		foreach ( $this->fields as $field) {
			echo '<div class="input-field">';
			echo $field;
			echo '</div> <!-- .input-field -->';
		}
		echo "
			<div class=\"form-buttons\">
				<input type=\"submit\" value=\"{$this->submitButtonText}\" />
				<input type=\"reset\" value=\"{$this->resetButtonText}\" />
			</div>
		";
		echo "</form>";
	}
	
	// GETTERS AND SETTERS
	
	public function setSubmitButtonText( $value) {
		$this->submitButtonText = $value;
	}
	
	public function setResetButtonText( $value) {
		$this->resetButtonText = $value;
	}
	
}

?>