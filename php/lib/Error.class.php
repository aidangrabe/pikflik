<?php
/**
 * Error.class.php
 * @author		Aidan Grabe
 * @version		20121202
 * @since		20121202
 */

class Error {
	
	private $level;
	private $message;
	private $tag;
	
	public function Error( $level, $tag, $message) {
		$this->level = $level;
		$this->tag = $tag;
		$this->message = $message;
	}
	
	public function getLevel() {
		return $this->level;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function getTag() {
		return $this->tag;
	}

}

?>