<?php
/**
 * ImageText.class.php
 * @author		Aidan Grabe
 * @version		20130315
 * @since		20121216
 * The ImageText class
 * A text object which uses the PHP GD library. You can get the text dimensions using this class
 */

require_once( "../../init.php");
require_once( "Error.class.php");

class ImageText {

	private $align;
	private $angle;
	private $color;
	private $corners;
	private $font;
	private $size;
	private $text;

	public function ImageText( $size, $text, $font, $align = "left", $angle = 0) {
		$this->align = $align;
		$this->angle = $angle;
		$this->color = 0x000;
		$this->corners = imagettfbbox( $size, $angle, $font, $text);
		$this->font = $font;
		$this->size = $size;
		$this->text = $text;
	}

	public function draw( $im, $x, $y) {
		$y += $this->getHeight();
		switch ( $this->align) {
			default:
			case "left":
				break;
			case "right":
				$x -= $this->getWidth();
				break;
			case "centre":
			case "center":
				$x -= $this->getWidth() / 2;
				break;
		};
		imagettftext( $im, $this->size, $this->angle, $x, $y, $this->color, $this->font, $this->text);
	}

	public function getColor() {
		return $this->color;
	}

	public function getHeight() {
		// lowerRight y - lowerLeft
		return $this->corners[1] - $this->corners[7];
	}

	public function getWidth() {
		// lowerRight x - lowerLeft x
		return $this->corners[2] - $this->corners[0];
	}

	public function setColor( $color) {
		$this->color = $color;
	}

}

?>