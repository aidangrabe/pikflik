<?php
/**
 * Data.class.php
 * @author		Aidan Grabe
 * @version		20121221
 * @since		20121202
 */
 
require_once( "Error.class.php");

class Data {
	
	private $data;
	private $errors;
	
	public function Data( &$data) {
		$this->data = $data;
		$this->errors = array();
	}
	
	public function getOptionalInt( $name, $min = 0, $max = PHP_INT_MAX, $default = null) {
		$int = $default;
		if ( isset( $this->data[ $name]) && trim( $this->data[ $name]) != '') {
			if ( (string) (int) $this->data[ $name] == trim( $this->data[ $name])) {
				if ( (int) $this->data[ $name] < $max && (int) $this->data[ $name] > $min) {
					$int = (int) $this->data[ $name];
				}
			}
		}
		return $int;
	}
	
	public function getOptionalString( $name, $minLength = 0, $maxLength = 255, $default = null) {
		$string = $default;
		if ( isset( $this->data[ $name]) && trim( $this->data[ $name]) != "") {
			$data = trim( $this->data[ $name]);
			if ( ! strlen( $data) < $maxLength && ( strlen( $data) > $minLength)) {
				$string = $data;
			}
		}
		return $string;
	}
	
	public function getErrors() {
		return $this->errors;
	}
	
	public function hasErrors() {
		return count( $this->errors) > 0 ? true : false;
	}
	
	public function getRequiredInt( $name, $min = 0, $max = PHP_INT_MAX) {
		$int = null;
		if ( isset( $this->data[ $name]) && trim( $this->data[ $name]) != '') {
			if ( (string) (int) $this->data[ $name] == trim( $this->data[ $name])) {
				if ( $int < $max && $int > $min) {
					$int = (int) $this->data[ $name];
				}
			}
		}
		return $int;
	}
	
	public function getRequiredString( $name, $minLength = 0, $maxLength = 255) {
		$string = null;
		if ( isset( $this->data[ $name]) && trim( $this->data[ $name]) != "") {
			$data = trim( $this->data[ $name]);
			if ( ! strlen( $data) < $maxLength && ( strlen( $data) > $minLength)) {
				$string = $data;
			} else {
				$this->errors[] = new Error( "Warning", get_class( $this), "{$name} should be between
												{$minLength} and {$maxLength} characters long.");
			}
		} else {
			$this->errors[] = new Error( "Warning", get_class( $this), "{$name} is a required string.");
		}
		return $string;
	}
	
	public static function stripScripts( $string) {
		while ( substr_count( $string, "<script>") > 0) {
			$string = str_replace( "<script>", "", $string);
		}
		while ( substr_count( $string, "</script>") > 0) {
			$string = str_replace( "</script>", "", $string);
		}
		return $string;
	}
	
}

?>