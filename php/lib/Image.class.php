<?php
/**
 * Image.class.php
 * @author		Aidan Grabe
 * @version		20130315
 * @since		20121205
 * The Image class
 * Creates a gd image which is scaleable and resizeable. Can be saved as multiple file-types.
 */

require_once( "../../init.php");
require_once( "Error.class.php");
require_once( "ImageText.class.php");

class Image {

	private $fontColor;
	private $image;
	private $type;
	private $errors;

	public function Image( $par) {
		$this->errors = array();
		if ( is_resource( $par) && get_resource_type( $par) == 'gd') {
			$this->image = $par;
		} else {
			$this->load( $par);
		}
		$this->fontColor = imagecolorallocate($this->image, 0, 0, 0);
	}

	/**
	 * @since 20121216
	 */
	public function addText($x, $y, $size, $string, $font, $align, $angle = 0) {
		$text = new ImageText( $size, $string, $font, $align, $angle);
		$text->setColor($this->fontColor);
		$text->draw( $this->image, $x, $y - $text->getHeight());
	}

	/**
	 * Displays the current image
	 */
	public function display() {
		header("Content-Type: image/jpeg");
		switch ($this->type) {
			default:
			case IMAGETYPE_JPEG:
				imageJPEG($this->image);
				break;
			case IMAGETYPE_PNG:
				imagePNG($this->image);
				break;
			case IMAGETYPE_GIF:
				imageGIF($this->image);
				break;
		}
	}

	public function getErrors() {
		return $this->errors;
	}

	public function getHeight() {
		return imagesy( $this->image);
	}

	public function getType() {
		return $this->type;
	}

	public function getWidth() {
		return imagesx( $this->image);
	}

	public function hasErrors() {
		return count( $this->errors) > 0 ? true : false;
	}

	public function load( $src) {
		$imageDetails = getImageSize( $src);
		if ( ! $imageDetails) {
			$this->errors[] = new Error( "Warning", get_class( $this), "Error getting image details");
		} else {
			$this->type = $imageDetails[2];
			switch ( $this->type) {
				default:
				case IMAGETYPE_JPEG:
					$this->image = imageCreateFromJPEG( $src);
					break;
				case IMAGETYPE_PNG:
					$this->image = imageCreateFromPNG( $src);
					break;
				case IMAGETYPE_GIF:
					$this->image = imageCreateFromGIF( $src);
					break;
			}
		}
	}

	public function resize( $width, $height) {
		$newImage = imageCreateTrueColor( $width, $height);
		imageCopyResampled( $newImage, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		return new Image( $newImage);
	}

	public function save( $filename, $extension = IMAGETYPE_JPEG, $compression = 80, $permissions = null) {
		switch ( $extension) {
			default:
			case IMAGETYPE_JPEG:
				imageJPEG( $this->image, $filename, $compression);
				break;
			case IMAGETYPE_PNG:
				imagePNG( $this->image, $filename);
				break;
			case IMAGETYPE_GIF:
				imageGIF( $this->image, $filename);
				break;
		}
		if ( $permissions != null) {
			chmod( $filename, $permissions);
		}
	}

	public function scale( $scale) {
		$width = $this->getWidth() * $scale / 100;
		$height = $this->getHeight() * $scale / 100;
		return $this->resize( $width, $height);
	}

	public function scaleToWidth( $newWidth) {
		$img = $this;
		if ( $newWidth < $this->getWidth()) {
			$height = $this->getHeight() / $this->getWidth() * $newWidth;
			$img = $this->resize( $newWidth, $height);
		}
		return $img;
	}

	public function setFontColor($r, $g, $b, $a) {
		$this->fontColor = imagecolorallocatealpha($this->image, $r, $g, $b, 127 - (127 * ($a / 100)));
	}

}

?>