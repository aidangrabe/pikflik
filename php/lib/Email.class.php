<?php
/**
 * Email.class.php
 * @author		Aidan Grabe
 * @version		20121202
 * @since		20121202
 */

 require_once( "Data.class.php");
 
class Email {
	
	private $body;
	private $errors;
	private $from;
	private $headers;
	private $subject;
	private $to;
	
	public function Email( $to, $from, $subject, $message, $headers = "") {
		$this->to = $to;
		$this->from = $from;
		$this->subject = $subject;
		$this->body = $message;
		$this->headers = $headers;
		
		$errors = array();
	}
	
	public function getErrors() {
		return $this->errors;
	}
	
	public function hasErrors() {
		return count( $this->errors) > 0 ? true : false;
	}
	
	public function send() {
		mail( $this->to, $this->subject, $this->body, $this->headers);
	}
	
	public function validate() {
		if ( get_magic_quotes_gpc( )) {
			$this->from = stripslashes( $this->from);
			$this->subject = stripslashes( $this->subject);
			$this->message = stripslashes( $this->message);
		}
		if ( ! Email::validateEmail( $this->from)) {
			$this->errors[] = new Error( "Warning", get_class( $this), "Please enter a valid email address");
		}
		$this->body = Data::stripScripts( $this->body);
	}
	
	public static function validateEmail( $email) {
		return preg_match( '/[A-Za-z0-9._]+@[A-Za-z0-9._]+.[A-Za-z0-9._]+/', $email) ? true : false;
	}
	
}

?>