<?php
/**
 * Page.class.php
 * @author		Aidan Grabe
 * @version		20121217
 * @since		20121130
 * A class designed to display the boiler-plate code of a standard HTML page
 */
class Page {
	
	private $charset;
	private $css;
	private $doctype;
	private $favicon;
	private $javascript;
	private $meta;
	private $title;
	
	public function Page( $title) {
		$this->charset = "utf-8";
		$this->css = array();
		$this->doctype = "html";
		$this->javascript = array();
		$this->favicon = "";
		$this->meta = "";
		$this->title = $title;
	}
	
	public function addCSS( $css) {
		$this->css[] = $css;
	}
	
	public function addJS( $js) {
		$this->javascript[] = $js;
	}
	
	public function addMeta( $meta) {
		$this->meta .= $meta;
	}
	
	public function outputFooter( ) {
		echo "
				</body>
			</html>
		";
	}
	
	public function outputHeader( ) {
		echo "<!DOCTYPE {$this->doctype}>
			<html>
				<head>
					<meta charset=\"{$this->charset}\" />
					<title>{$this->title}</title>";
					foreach ( $this->css as $stylesheet) {
						echo "<link rel=\"stylesheet\" href=\"{$stylesheet}\" />";
					}
					foreach ( $this->javascript as $script) {
						echo "<script type=\"text/javascript\" src=\"{$script}\"></script>";
					}
					if ( $this->favicon != "") {
						echo "<link rel=\"shortcut icon\" type=\"image/png\" href=\"{$this->favicon}\" />";
					}
		echo "
				{$this->meta}
				</head>
				<body>
		";
	}
	
	public function outputNav() {
		//
	}
	
	// GETTERS AND SETTERS
	
	public function getTitle( ) {
		return $title;
	}
	
	public function setCharset( $charset) {
		$this->charset = $charset;
	}
	
	public function setDoctype( $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * Sets the shortcut icon to the given source
	 * @param String $src the path to the .ico file
	 * @since 20121217
	 */
	public function setFavicon( $src) {
		$this->favicon = $src;
	}
	
}

?>