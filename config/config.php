<?php
/**
 * config.php
 * @author		Aidan Grabe
 * @version		20130315
 * @since		20121208
 * User config
 */

define("ALBUM_THEME", 'facebook2');
define("ALBUM_TITLE", file_get_contents( PATH . "config/title.txt"));
define("ALBUM_DESCRIPTION", file_get_contents( PATH . "config/description.html"));
define("THUMBNAIL_WIDTH", 178);
define("DISPLAY_WIDTH", 720);

// the transparency of the text written on the image
define("IMAGE_FONT_ALPHA", 75);	// 0: transparent; 100: opaque

// the size of the font written on the image
define("IMAGE_FONT_SIZE", 7);

// the .ttf font file
define("IMAGE_FONT", PATH . "css/fonts/coolvetica.ttf");

// the image text to write to the image
define("IMAGE_COPYRIGHT_TEXT", ALBUM_TITLE);

?>