<?php
/**
 * FaceBook.class.php
 * @author		Aidan Grabe
 * @version		20121217
 * @since		20121208
 * A Facebook theme
 */

require_once( PATH_LIB . "Page.class.php");

class FaceBookPage extends Page {
	
	public function FaceBookPage() {
		parent::__construct( ALBUM_TITLE);
		$this->addCSS( ROOT_THEME . "css/facebook.css");
		$this->setFavicon( ROOT_THEME . "img/favicon.ico");
	}
	
	public function outputFooter() {
		echo "
			</div> <!-- .container -->
			<footer>
				<div class=\"container\">
					Copyright " . ALBUM_TITLE . " &copy; " . date( 'Y') . "
				</div> <!-- .container -->
			</footer>
		";
		parent::outputFooter( );
	}
	
	public function outputHeader() {
		parent::outputHeader( );
		echo "
			<header>
				<div class=\"container\">
					<h1>" . ALBUM_TITLE . "</h1>
				</div> <!-- .container -->
			</header>
			<div id=\"main-container\" class=\"container\">
		";
	}
	
}


?>