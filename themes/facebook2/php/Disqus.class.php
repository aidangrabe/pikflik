<?php
/**
 * Disqus.class.php
 * @author		Aidan Grabe
 * @version		20121215
 * @since		20121215
 * 
 */

class Disqus {
	
	private $shortname;
	
	public function Disqus( $shortname) {
		$this->shortname = $shortname;
	}
	
	/**
	 * output the actual script which will load the comments
	 */
	public function output() {
		echo "
			<div id=\"disqus-comments\">
				<div id=\"disqus_thread\"></div>
			    <script type=\"text/javascript\">
			        var disqus_shortname = '{$this->shortname}';
			        (function() {
			            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			            dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			        })();
			    </script>
			    <noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>
			    <a href=\"http://disqus.com\" class=\"dsq-brlink\">comments powered by <span class=\"logo-disqus\">Disqus</span></a>
			</div> <!-- #disqus-comments -->
	    ";
	}
}

?>