<?php

/**
 * init.js.php
 * @author		Aidan Grabe
 * @version		20130126
 * @since		20130116
 * This script will set up the javascript variables for use by the index page.
 */

require_once( "../../../init.php");
require_once( PATH_LIB . "Data.class.php");
require_once( PATH_PIK . "Sync.class.php");
 
header( "Content-type: text/javascript");

$images = Sync::getImages( IMG_DISPLAY);
$data = new Data($_GET);
$currentImage = $data->getOptionalString( 'img', 0, 500, null);

// Output some constants
echo "var HOST_URL = 'http://" . $_SERVER['HTTP_HOST'] . ROOT . "';";
echo "var THEME_ROOT = '" . ROOT . THEME_ROOT . "facebook2/';";
echo "var IMG_DISPLAY = '" . ROOT . IMG_DISPLAY . "';";
echo "var IMG_THUMBNAIL = '" . ROOT . IMG_THUMBNAIL . "';";
echo "var CURRENT_IMG = '" . ($currentImage != null
					? "{$currentImage}"
					: "")
	. "';";

// Output the image file names
$separator = "";
echo "var imageNames = [";
foreach ( $images as $src) {
	echo "{$separator}'{$src}'";
	$separator = ",";
}
echo "];";

?>