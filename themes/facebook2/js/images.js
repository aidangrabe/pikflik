/**
 * images.js
 * @author		Aidan Grabe
 * @version		20130131
 * @since		20121208
 * This script creates/displays the thumbnails
 */

function MainImage( ) {
	
	var container;
	var currentImage = '';
	var horAd = $('#hor-ad');
	var imageContainer;
	var imageTitleContainer;
	var textContainer;
	
	shadow = $('<div class="shadow"></div>');
	$("body").append( shadow);
	container = $('<div class="container"><div class="text"></div><div class="image"></div></div>');
	$("body")
		.append(container)
		.append(horAd);
	horAd.css({
		background: '#FFF',
		height: 0,
		left: '50%',
		overflow: 'hidden',
		position: 'fixed',
		top: '50%'
	});
	// the left + right buttons
	var imageButtons = $('<div><div class="left-button"><img src="' + THEME_ROOT + 'img/left-arrow.png" /></div><div class="right-button"><img src="' + THEME_ROOT + 'img/right-arrow.png" /></div></div>');
	$("body").append(imageButtons);
	imageButtons.css({
		display: 'none',
		left: '50%',
		position: 'fixed',
		top: '50%'
	})
	.mouseenter(function() {
		imageButtons.find(".left-button , .right-button").each(function() {
			$(this).fadeIn();
		});
	})
	.mouseleave(function() {
		imageButtons.find(".left-button , .right-button").each(function() {
			$(this)
				.stop()
				.fadeOut();
		});
	});
	imageButtons.children('.left-button')
		.click(function() {
			previousImage();
		})
		.css({
			cursor: 'pointer',
			display: 'none',
			float: 'left'
		});
	imageButtons.children('.right-button')
		.click(function() {
			nextImage();
		})
		.css({
			cursor: 'pointer',
			display: 'none',
			float: 'right'
		});
	imageContainer = container.children(".image");
	textContainer = container.children(".text");
	container.css({
		background: '#FFF',
		border: '1px solid #DDD',
		display: 'none',
		left: '50%',
		position: 'fixed',
		top: '50%'
	});
	textContainer.css( {
		float: 'right',
		width: 300
	});
	shadow.css({
		background: 'rgba( 0, 0, 0, .6)',
		bottom: 0,
		display: 'none',
		left: 0,
		position: 'fixed',
		right: 0,
		top: 0
	});
	// fade out the whole large image container
	shadow.click( function() {
		horAd.animate({
			height: 0
		});
		container.fadeOut();
		shadow.fadeOut();
		imageButtons.css('display', 'none');
		imageTitleContainer.fadeOut();
	});
	
	// this container will hold the image title
	imageTitleContainer = $('<div id="image-title"></div>');
	$("body").append(imageTitleContainer);
	
	var getImageIndex = function(src) {
		var imageSource = src.substring(src.lastIndexOf("/") + 1, src.length);
		for (i=0; i<imageNames.length; i++) {
			if (imageNames[i] == imageSource) {
				return i;
			}
		}
	}
	
	var nextImage = function() {
		var imageSource = currentImage;
		var imageIndex = getImageIndex(imageSource);
		var nextImageIndex = (imageIndex + 1) % imageNames.length;
		show(IMG_DISPLAY + imageNames[nextImageIndex]);
	}
	
	var previousImage = function() {
		var imageSource = currentImage;
		var imageIndex = getImageIndex(imageSource);
		var previousImageIndex = (((imageIndex - 1) % imageNames.length) + imageNames.length) % imageNames.length;
		show(IMG_DISPLAY + imageNames[previousImageIndex]);
	}
	
	
	var show = function( imgSrc) {
		currentImage = imgSrc;
		imageContainer.children("img").remove();
		var newImage = $('<img src="" />');
		newImage.attr( "src", imgSrc);
		newImage.bind( "load", function() {
			container.css( {
				display: 'block',
				visibility: 'hidden'
			});
			if ( imageContainer.children("img").outerHeight() > $(window).innerHeight() - 40) {
				imageContainer.children("img").css({
					height: $(window).innerHeight() - 40
				});
			}
			container.css( {width: textContainer.outerWidth() + imageContainer.children("img").outerWidth()});
			textContainer.html( '<iframe src="' + THEME_ROOT + 'php/disqus.php?url='+ imgSrc +'"></iframe>');
			textContainer.append( '<div class="share-buttons"><img src="' + THEME_ROOT + 'img/share.png" class="share" title="Share!" /></div>');
			var shareContainer = textContainer.children(".share-buttons");
			shareContainer.css({
				float: 'right'
				})
				.children(".share")
					.css('cursor', 'pointer')
					.click(function() {
					shareContainer.children("textarea").remove();
					var textLink = $('<textarea>' + HOST_URL + '?img=' + imgSrc + '</textarea>');
					shareContainer.append( textLink);
					textLink.css({
						border: 0,
						float: 'right',
						height: 32,
						margin: 0,
						padding: 0,
						width: 0
					});
					textLink.animate({
						width: textContainer.innerWidth() - 32
						}, function() {
							this.focus();
							this.select();
						});
				});
			textContainer.children("iframe").css({
				border: 0,
				height: container.innerHeight() - 32,
				margin: 0,
				width: textContainer.innerWidth()
			});
			var containerWidth = container.outerWidth();
			var containerHeight = container.outerHeight();
			container.css({
				marginLeft: -containerWidth / 2,
				marginTop: -containerHeight / 2,
				visibility: 'visible',
				display: 'none'
			});
			shadow.fadeIn();
			container.fadeIn();
			var imWidth = imageContainer.children("img").outerWidth();
			horAd.css({
				marginLeft: -containerWidth / 2,
				marginTop: containerHeight / 2,
				width: imWidth > 728 ? imWidth : 728
			});
			horAd.animate({
				height: 90
			}, 'slow');
			imageButtons
				.css({
					height: containerHeight - (containerHeight / 2 - 55),
					marginLeft: -containerWidth / 2,
					marginTop: -containerHeight / 2,
					paddingTop: containerHeight / 2 - 55,
					width: imWidth
				})
				.fadeIn();
			// display the title of the image
			var imageTitle = imgSrc.substring(imgSrc.lastIndexOf("/") + 1, imgSrc.lastIndexOf("."));
			imageTitleContainer.html("<span>" + imageTitle + "</span>");
			imageTitleContainer.css({
				top: 0,
				position: "fixed",
				textAlign: "center",
				width: "100%"
			});
			imageTitleContainer.children("span").css({
				background: "#FFF",
				border: "1px solid #000",
				borderBottomLeftRadius: "10px",
				borderBottomRightRadius: "10px",
				borderTop: "none",
				display: "table",
				margin: "auto",
				padding: "10px"
			});
	});
		imageContainer.append( newImage);
	}
	
	return {
		show: show
	}
	
}

/**
 * The thumbnails object allows you to display an array of images as thumbnails
 * @param BigImage bigImage the bigImage object containing the large image to show
 * @param String container the jQuery ID of the container that will hold the thumbnails
 * @param Array<String> images an array of image sources
 */
function Thumbnails( container, imageSources) {
	
	var mainImage = new MainImage( );
	var pageContainer;
	var self = this;
	var thumbnailLinks;
	var thumbnails;
	var thumbsBorderWidth = 1;
	var thumbsMargin = 8;
	var thumbsPadding = 4;
	var thumbsPerCol = 5;
	var thumbsPerRow = 4;
	
	var create = function( page) {
		// get the container
		container = $(container);
		container.html( "");
		// get
		var thumbsPerPage = thumbsPerCol * thumbsPerRow;
		for ( imgIndex = (page-1) * thumbsPerPage; imgIndex < page * thumbsPerPage
				&& imgIndex < imageSources.length; imgIndex++) {
			var imgName = imageSources[imgIndex];
			var imgThumb = IMG_THUMBNAIL + imgName;
			var imgDisplay = IMG_DISPLAY + imgName;
			container.append('<a href="' + imgDisplay + '"><img src="' + imgThumb + '"></a>');
		}
		thumbnailLinks = container.children( "a");
		thumbnails = thumbnailLinks.children( "img");
		thumbnailLinks.bind( "click", function( event) { onThumbnailClick( event, this);});
		$(window).resize( resize);
		createPageContainer(thumbsPerPage);
		resize();
	};
	
	var createPageContainer = function(thumbsPerPage) {
		var html = '';
		var numPages = Math.ceil(imageSources.length / thumbsPerPage);
		for (i = 0; i < numPages; i++) {
			html += '<span>' + (i + 1) + '</span>';
		}
		html = '<div>' + html + '</div>';
		pageContainer = $(html);
		pageContainer
			.css({
				clear: 'both',
				padding: '20px 0',
				textAlign: 'center'
			})
			.children("span")
				.css({
					background: '#CCC',
					border: '1px solid #AAA',
					borderRadius: '2px',
					cursor: 'pointer',
					margin: '0 5px',
					padding: '4px 8px'
				})
				.click(function() {
				create($(this).html());
			});
		container.append(pageContainer);
	}
	
	var onThumbnailClick = function( event, self) {
		event.preventDefault();
		var url = $(self).attr( "href");
		mainImage.show( url);
	}
	
	var resize = function() {
		// resize the thumbnails to fit the container
		var containerWidth = container.innerWidth() - thumbsMargin;
		var width = Math.floor( containerWidth / thumbsPerRow) - thumbsMargin
				- thumbsPadding * 2 - thumbsBorderWidth * 2;
		thumbnailLinks.css( {
			border: "1px solid #c4cde0",
			borderWidth: thumbsBorderWidth,
			display: "block",
			float: "left",
			height: width / 3 * 2,
			margin: thumbsMargin,
			marginRight: 0,
			marginBottom: 0,
			padding: thumbsPadding,
			width: width
		});
		var width = thumbnailLinks.innerWidth() - 1 - thumbsPadding * 2;
		thumbnails.css( {
			height: width / 3 * 2,
			width: width
		});
		container.css( {
			overflow: "hidden",
			paddingBottom: thumbsPadding
		});
	};
	
	var showLargeImage = function(src) {
		mainImage.show(src);
	}
	
	return {
		create: create,
		showLargeImage: showLargeImage
	}
	
};

$(document).ready( function() {

	var thumbnails = new Thumbnails( "#thumbnails", imageNames);
	thumbnails.create( 1);
	if ( CURRENT_IMG != '') {
		thumbnails.showLargeImage(CURRENT_IMG);
	}
	
});