<?php
/**
 * index.php
 * @author		Aidan Grabe
 * @version		20130114
 * @since		20121208
 * A Facebook theme
 */

require_once( PATH_LIB . "Data.class.php");
require_once( "php/FaceBookPage.class.php");
require_once( "php/Disqus.class.php");

$showComments = true;

$data = new Data( $_GET);
$currentImage = $data->getOptionalString( "img", 0, 500, "");

$page = new FaceBookPage();
$page->addJS( ROOT . "js/jquery.js");
$page->addJS( ROOT . "php/pikflik/theme.js.php?img={$currentImage}");
$page->addJS( ROOT_THEME . "js/images.js");
$page->outputHeader();

$shareUrl = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
?>

<aside>
	<div class="ad">
		<script type="text/javascript"><!--
            google_ad_client = "pub-7155751137153447";
            /* TaxCalcV 160x600 */
            google_ad_slot = "2095202127";
            google_ad_width = 160;
            google_ad_height = 600;
            //-->
        </script>
        <script type="text/javascript"
            src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
		<script type="text/javascript"><!--
            google_ad_client = "pub-7155751137153447";
            /* TaxCalcV 160x600 */
            google_ad_slot = "2095202127";
            google_ad_width = 160;
            google_ad_height = 600;
            //-->
        </script>
        <script type="text/javascript"
            src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
	</div>
</aside>

<section id="main-content">
	<section id="description">
		<!-- FACEBOOK LIKE BUTTON -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=216703681730061";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<!-- TWITTER BUTTON -->
		<script>
			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id))
			{js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
		</script>
		<!-- DISPLAY BOTH BUTTONS -->
		<div class="social">
			<div><a href="https://twitter.com/share?count=horizontal&url=<?php echo $shareUrl; ?>&text=<?php echo ALBUM_TITLE . " " . $shareUrl; ?>" class="twitter-share-button" data-lang="en">Tweet</a></div>
			<div class="fb-like" data-href="<?php echo $shareUrl; ?>"
				 data-send="false" data-layout="button_count" data-width="32"
				 data-show-faces="true" data-font="trebuchet ms">
			</div>
		</div> <!-- .social -->
		<!-- END FACBOOK LIKE BUTTON -->
		<div class="ablum-description">
			<?php echo ALBUM_DESCRIPTION; ?>
		</div>
	</section>
	<section id="images">
	</section>
	<div id="pages"></div> <!-- #pages -->
	<?php
		// show the comments if necessary
		if ( $showComments) {
			$disqus = new Disqus( "aidangrabecom");
			$disqus->output();
		}
	?>
</section> <!-- #main-content -->

<div id="ad-popup">
	<div class="close">X</div>
	<script type="text/javascript"><!--
		google_ad_client = "pub-7155751137153447";
		google_ad_width = 728;
		google_ad_height = 90;
		google_ad_format = "728x90_as";
		google_ad_type = "text_image";
		google_ad_channel = "";
//--></script>
    <script type="text/javascript"
	  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
	<!-- The close button for the ad -->
</div> <!-- #ad-popup -->

<?php
$page->outputFooter();
?>
