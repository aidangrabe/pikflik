/**
 * images.js
 * @author		Aidan Grabe
 * @version		20130114
 * @since		20121208
 * This script creates/displays the thumbnails
 */
function BigImage( shadow, container) {
	var html = "<div id=\"shadow\"></div><div id=\"bigImage\"><span>Hello</span><img src=\"" + window.images_display[0] + "\"></div>";
	var that = this;
	$("body").append( html);
	$(shadow)
		.click( function() {
			that.hide();
		})
		.css( { display: "none"});
	$(container).css( { display: "none"});
	$(container).click( function() {
		$(container + " img").fadeOut( function() {
			that.set( window.images_display[ that.getNextImageIndex( )]);
		});
	});
	var popUpAd = $( "#ad-popup");
	// close the ad if the close button is clicked
	$( "#ad-popup .close").click( function() {
		popUpAd.fadeOut();
	});
	
	// pop an alert box with link to image when right clicked
	$(container).bind( 'contextmenu', function( event) {
		event.preventDefault();
		alert( window.HOST + window.ROOT + "?img=" + that.getCurrentImage());
	});
	
	/**
	 * @return the 'src' attribute of the bigImage
	 */
	this.getCurrentImage = function() {
		return $( container + " img").attr( "src");
	}
	
	/**
	 * Gets relative link to the display sized image of the given file.
	 * @param file a relative link to an image in either the source or thumbnail folder
	 * @return String the link to the display image relative to the root of the host
	 */
	this.getDisplayName = function( file) {
		file = window.ROOT + window.IMG_DISPLAY + this.getFileName( file);
		return file;
	}
	
	/**
	 * Strips directories from the given file path
	 * @param file the path to a file
	 * @return String the file name ( including file extension ) of the given file
	 */
	this.getFileName = function( file) {
		return file.substr(file.lastIndexOf( "/") + 1);
	}
	
	/**
	 * Get the index of the next image to display
	 * Gets the current image, checks the image array to see what the current index is, and gets the 
	 * next logical index.
	 * If the next index is out of the range of the image array, the index will be reset to 0, ie.
	 * it will loop back around to the first image.
	 * @return int the index of the next image ( 0 <= i < window.images_display.length )
	 */
	this.getNextImageIndex = function( ) {
		var currentImage = that.getCurrentImage();
		var index = 0;
		var images = window.images_display;
		for ( i = 0; i < images.length; i++) {
			if ( currentImage == images[i]) {
				index = i;
				break;
			}
		}
		if ( index == images.length - 1) {
			index = 0;
		} else {
			index++;
		}
		return index;
	}
	
	/**
	 * Fades the shadow and bigImage container out
	 */
	this.hide = function() {
		$(shadow).fadeOut();
		$(container).fadeOut();
		popUpAd.fadeOut();
	}
	
	/**
	 * repositions the bigImage image to the center of the window
	 */
	this.positionImage = function() {
		var loadingSpan = $(container + " span");
		if ( loadingSpan.width() != 0) {
			loadingSpan.css({
				marginLeft: - loadingSpan.width() / 2,
				visibility: 'visible'
			});
		}
		$(container + " img").load( function() {
			$(this).css( {
				marginLeft: - $(this).outerWidth() / 2,
				marginTop: - $(this).outerHeight() / 2
			});
		});
	}
	
	/**
	 * Sets the source attribute of the bigImage image
	 * NOTE: does not display the bigImage, simply sets the source
	 * @param source the new source to set the attribute to, must be an image
	 */
	this.set = function( source) {
		$(container + " img").remove();
		$(container).append( "<img src=\"" + this.getDisplayName( source) + "\" />");
		$( container + " span").remove();
		$( container).prepend( "<span>Loading...</span>");
		$( container + " img").css( { visibility: 'hidden'});
		this.positionImage();
		$( container + " img").css( { visibility: 'visible'});
		$( container + " span").fadeIn();
		$( container + " img").fadeIn();
		popUpAd.fadeIn();
	}
	
	/**
	 * Fades the shadow and bigImage in, and repositions the bigImage image
	 */
	this.show = function() {
		$(shadow).fadeIn();
		$(container).fadeIn( "slow");
	}
}

/**
 * The thumbnails object allows you to display an array of images as thumbnails
 * @param BigImage bigImage the bigImage object containing the large image to show
 * @param String container the jQuery ID of the container that will hold the thumbnails
 * @param Array<String> images an array of image sources
 */
function Thumbnails( bigImage, container, images) {
	var currentPage = 1;
	var numPerPage = 20;
	var numPerRow = 4;
	var html = "";
	var pageNumberContainer = "#pages";
	var that = this;
	for ( i = 0; i < images.length / numPerPage; i++) {
		html += "<span>" + ( i + 1) + "</span>";
	}
	$(pageNumberContainer).html( html);
	$(pageNumberContainer + " span").each( function() {
		$(this).click( function() {
			currentPage = $(this).html();
			that.display();
		});
	});
	
	/**
	 * Displays the current page of thumbnails by filling the container with new <img>
	 * tags.
	 * Also attaches an onclick event to each thumbnail which will trigger the bigImage
	 * with the source of the current thumbnail
	 */
	this.display = function( ) {
		var start = numPerPage * ( currentPage - 1);
		var end = numPerPage * currentPage;
		var html = "";
		for ( i = start; i < images.length && i < end; i++) {
			html += '<div><img src="' + images[i] + '" /></div>';
		}
		$( container).html( html);
		this.resize();
		$(container + " img").click( function() {
			var imageSource = $(this).attr( "src");
			bigImage.set( imageSource);
			bigImage.show();
		});
	}
	
	/**
	 * Resize the thumbnails
	 * the width of each thumbnail is calculated using percentages in the CSS.
	 * percentages are dynamic, but cannot be used for heights.
	 * this method resizes each thumbnail's height to n/m the width of each thumbnail
	 * NOTE: the new height is calculated only once, and each thumbnail gets this height
	 * 		 this is due to the fact that percentages give floating point widths, and not
	 * 		 all heights were equal when calculated individually leading to weird layout bugs.
	 */
	this.resize = function() {
		var n = 3;
		var m = 2;
		var margin = 3.75;
		var imageBorder = 1;
		var imagePadding = 5;
		var containerWidth = $( container).innerWidth() - ( margin * 2);
		var thumbWidth = containerWidth / numPerRow - ( margin * 2);
		var thumbHeight = thumbWidth / n * m;
		$( container).css( { padding: margin + "px 0 0 " + margin + "px"});
		$( container + " > div").css( {
			width: thumbWidth,
			height: thumbHeight,
			margin: margin
		});
		$( container + " > div img").css( {
			width: thumbWidth - imageBorder * 2 - imagePadding * 2,
			height: thumbHeight - imageBorder * 2 - imagePadding * 2,
			borderStyle: 'solid',
			borderWidth: imageBorder,
			padding: imagePadding
		});
	}
}

$(document).ready( function() {
	
	// create the bigImage and thumbnails
	var bigImage = new BigImage( "#shadow", "#bigImage");
	var thumbs = new Thumbnails( bigImage, "#images", images_thumbnails);
	
	// display the thumbnails
	thumbs.display( );
	
	// when the window gets resized, resize the thumbnails
	$(window).resize( function() {
		thumbs.resize();
	});
	
	// 
	if ( IMG_CURRENT != "") {
		bigImage.set( IMG_CURRENT);
		bigImage.show();
	}
	
});