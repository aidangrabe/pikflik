<?php
/**
 * index.php
 * @author		Aidan Grabe
 * @version		20121208
 * @since		20121208
 * The main page
 */

require_once( "init.php");
require_once( PATH_PIK . "Theme.class.php");

$theme = new Theme( ALBUM_THEME);
$theme->output();

?>