<?php
/**
 * index.php
 * @author		Aidan Grabe
 * @version		20121217
 * @since		20121205
 * The admin page
 */

require_once( "../init.php");
require_once( PATH_PIK . "PikFlikAdminPage.class.php");

$page = new PikFlikAdminPage( PROJECT_TITLE . " Admin");
$page->addJS( ROOT . "js/jquery.js");
$page->addJS( ROOT . "php/pikflik/images.js.php?time=" . time());		// append time to force no-cache
$page->addJS( ROOT . "js/sync.js");
$page->outputHeader();

?>

Welcome to the Admin homepage!

<div id="sync-container">
	<div>
		<input type="button" value="Sync" id="sync-button" />
	</div>
	<div>
		<div class="loading">
			<div></div>
		</div>
	</div>
</div>
<!-- #sync-container -->

<p>
	<a href="<?php echo ROOT; ?>" class="clickable">View album</a>
</p>

<?php
$page->outputFooter();
?>